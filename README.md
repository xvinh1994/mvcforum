This is my project in free time. I developed this project in ASP.NET MVC in order to learn MVC and some of javascript. The reason I choose a forum as my project because it sounds interesting. Many things are combined in one forum but right now I have built some functions that usually exists in a forum.

--------------v1.0------------------
Simple UI using bootstrap, with my little knowledge about CSS the interface is not attractive. 
Show what I want from database on UI.
Using JS for clock which is searched on internet.

--------------v1.1------------------
Build a custom login page with the help from web **CodeProject**.
Login successfully and able to restrict what web user could do.

--------------v1.2------------------
Build custom login by Facebook with **Facebook C# SDK** and get stuck with the response from *Facebook*.
Using **Google C# SDK** to create custom login using services of *Google* but some hosts that I used to test need access to read file which I can't change permission.
Using **TweetSharp** and developed login successfully without error.

--------------v1.3------------------
Fix all error in Facebook login and all things worked, no more error.
Change code for custom external login (Facebook, Google, Twitter) and move to separate file to make code clear and understandable.

--------------v1.4------------------
Develop simple function to post a new thread.
Develop simple function to post in a thread.
All data was stored in database. Thinking if it's good choice

--------------v1.4.1------------------
Add simple tools to **Bold**, *Italic* and Underline, change font or size of string with javascript by adding some HTML code directly.
Tools worked well and I also found out that if I use HTML directly, there is chance that my forum can be attacked by anyone who know HTML.

--------------v1.4.2------------------
Find out way to solve the risk of using HTML directly by changing HTML code into non-HTML tags such as [B] for **Bold** or [I] for *Italic*, etc.
Regular expression brought a powerful tools to actualize my idea. Now data user input can't contain HTML tags.

--------------v1.5------------------
Decide to store data of post into HTML files. All comments posted will be store in separate HTML files on server. Database server stores URL to these files

--------------v1.5------------------
Add simple admin tools: ban user, edit password or grant access for a user.
Cache is applied in purpose of improving performance.