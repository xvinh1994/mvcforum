create trigger AutoAddData
on Member
after insert
as
begin
	declare @id int
	select @id=id from inserted
	insert into Membership values(@id, 'User')
	insert into MemberInfo values(@id, 0, 0, 0, 0, getdate())
end