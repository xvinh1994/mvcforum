create table Category(
 shortname char(50) primary key not null,
 fullname nvarchar(max) not null,
 urlLink nchar(50) not null,
 needAuthenticate bit null default 0
);

create table Member(
 id int not null primary key identity(1,1),
 username char(50) not null unique,
 pword char(50) not null,
 avatarUrl nvarchar(max) null,
 banned bit null default 0
);

create table Membership(
 memID int primary key foreign key references Member(id),
 privileges nchar(50)
);

create table Privilege(
 shortAccess nchar(50) primary key,
 accessLevel nvarchar(max),
 priority int default 0 check (priority >=0 and priority <= 5)
);

create table MemberInfo(
 memID int not null primary key foreign key references Member(id),
 likes int default 0,
 dislikes int default 0,
 thanks int default 0,
 numPosts int default 0,
 dateReg date null default(getdate())
);

create table PostIcon(
 id int not null primary key identity(0, 1),
 iconUrl nvarchar(max) default '#'
);

create table Post(
 id int not null primary key identity(1, 1),
 categoryID char(50) foreign key references Category(shortname),
 postMem int foreign key references Member(id),
 postName nvarchar(max),
 datePost date null default(getdate()),
 numView int default 1,
 postIconID int foreign key references PostIcon(id),
 dataDirectory nvarchar(max)
);

create table Comment(
 id int not null primary key identity(1, 1),
 commentMem int foreign key references Member(id),
 commentPost int foreign key references Post(id),
 commentData nvarchar(max),
 dateComment date default(getdate()),
 replyComment int null foreign key references Comment(id)
);

insert into Category values('PubOne', 'Public One', '~/Category/PubOne', 0);
insert into Category values('PubTwo', 'Public Two', '~/Category/PubTwo', 0);
insert into Category values('PriOne', 'Private One', '~/Category/PriOne', 1);
insert into Category values('PriTwo', 'Private Two', '~/Category/PriTwo', 1);
insert into Category values('PriThree', 'Private Three', '~/Category/PriThree', 1);

insert into Member values('admin', '123456', '', 0);
insert into Member values('userA', '123456', '', 0);
insert into Member values('userB', '123456', '', 0);
insert into Member values('bannedUser', '123456', '', 1);

insert into Privilege values('Admin', 'Administrator', 5);
insert into Privilege values('User', 'User', 0);
insert into Privilege values('Mod', 'Moderator', 3);

insert into Membership values(1, 'Administrator');
insert into Membership values(2, 'User');
insert into Membership values(3, 'User');
insert into Membership values(4, 'User');

insert into MemberInfo values(1, 0, 0, 0, 0, getdate());
insert into MemberInfo values(2, 0, 0, 0, 0, getdate());
insert into MemberInfo values(3, 0, 0, 0, 0, getdate());
insert into MemberInfo values(4, 0, 0, 0, 0, getdate());

insert into PostIcon values('#');
insert into PostIcon values('~/img/icons/icon1.png');
insert into PostIcon values('~/img/icons/icon2.png');
insert into PostIcon values('~/img/icons/icon3.png');
insert into PostIcon values('~/img/icons/icon4.png');
insert into PostIcon values('~/img/icons/icon5.png');
insert into PostIcon values('~/img/icons/icon6.png');
insert into PostIcon values('~/img/icons/icon7.png');
insert into PostIcon values('~/img/icons/icon8.png');
insert into PostIcon values('~/img/icons/icon9.png');
insert into PostIcon values('~/img/icons/icon10.png');
insert into PostIcon values('~/img/icons/icon11.png');
insert into PostIcon values('~/img/icons/icon12.png');
insert into PostIcon values('~/img/icons/icon13.png');
insert into PostIcon values('~/img/icons/icon14.png');
