﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumModel;
using System.Web.Security;

namespace ForumController.Controllers
{
    public class MemberController : Controller
    {
        [OutputCache(Duration = 300, Location = System.Web.UI.OutputCacheLocation.Client, VaryByParam = "id", NoStore = true)]
        public ActionResult FindMemberByID(int id)
        {
            ForumDBEntities db = new ForumDBEntities();
            Member mem = db.Members.SingleOrDefault(x => x.id == id);

            if (mem == default(Member))
            {
                ViewBag.ErrorMessage = "Member not found!!!";
                return View("Error");
            }
            
            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) ViewBag.IsCurrentUserAdmin = false;
            else
            {
                string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                ViewBag.IsCurrentUserAdmin = db.Members.Single(x => x.username == username).Membership.privileges.Trim() == "Administrator" ? true : false;
            }

            return View("MemberInfo", mem);
        }
        
        public ActionResult ShowMember(int id)
        {
            ViewBag.MemberID = id;
            return View();
        }

        [OutputCache(Duration = 10, Location = System.Web.UI.OutputCacheLocation.Client, VaryByParam = "pattern", NoStore = true)]
        public ActionResult FindAllMember(string pattern)
        {
            if(!string.IsNullOrEmpty(pattern) || !string.IsNullOrWhiteSpace(pattern))
            {
                ForumDBEntities db = new ForumDBEntities();
                var model = db.Members.Where(x => x.username.ToLower().Contains(pattern.ToLower()));

                return View(model);
            }
            return new EmptyResult();
        }
    }
}