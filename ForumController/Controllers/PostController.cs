﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumModel;
using System.Web.Security;
using ForumController.ExternalClasses;

namespace ForumController.Controllers
{
    public class PostController : Controller
    {
        [OutputCache(Duration = 300, Location = System.Web.UI.OutputCacheLocation.Client, VaryByParam = "*", NoStore = true)]
        public ActionResult GetPostByMember(int memberID, int page)
        {
            ForumDBEntities db = new ForumDBEntities();
            var posts = db.Posts.Where(x => x.Member.id == memberID).OrderByDescending(x => x.datePost);

            if (posts.Count() == 0)
                return View(new List<Post>());

            var model = posts.Skip((page - 1) * 10).Take(10);

            return View(model);
        }

        [OutputCache(Duration = 300, Location = System.Web.UI.OutputCacheLocation.Server, VaryByParam = "*", NoStore = true)]
        public ActionResult GetPostByCategory(string categoryID, int page)
        {
            ForumDBEntities db = new ForumDBEntities();
            var posts = db.Posts.Where(x => x.categoryID == categoryID).OrderByDescending(x => x.datePost);
            ViewBag.CategoryID = categoryID;

            if (posts.Count() == 0)
                return View(new List<Post>());

            var model = posts.Skip((page - 1) * 10).Take(10);

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Create(string categoryID)
        {
            ForumDBEntities db = new ForumDBEntities();
            if (!db.Categories.Any(x => x.shortname == categoryID))
            {
                ViewBag.ErrorMessage = "The category " + categoryID + " is not available on this forum.";
                return View("Error");
            }

            string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
            int userid = db.Members.Single(x => x.username == username).id;

            Post post = new Post();
            Comment cmt = new Comment();
            var model = new ThreadPost() { post = post, comment = cmt };
            ViewBag.CategoryName = db.Categories.Single(x => x.shortname == categoryID).fullname;
            ViewBag.CategoryID = categoryID;
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateInput(false)]
        public ActionResult Create(string categoryID, ThreadPost model, int selectedIconID)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Please fulfill all needed data!!!");
                return View(model);
            }

            ForumDBEntities db = new ForumDBEntities();
            string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
            int userid = db.Members.Single(x => x.username == username).id;
            
            Post post = new Post() { categoryID = categoryID, datePost = DateTime.Now.Date, postMem = userid, postName = model.post.postName, postIconID = selectedIconID, numView = 0 };
            Comment cmt = new Comment() { commentMem = userid, commentPost = post.id, dateComment = DateTime.Now.Date, commentData = model.comment.commentData };

            CommentHelper helper = new CommentHelper();
            string filename = helper.GetFileName(cmt.id);
            string mapPath = helper.CreateDirectoryForNewPost(post.id, Server);
            string fullPath = mapPath + "/" + filename;
            bool isOk = helper.WriteDataToHTML(Server.MapPath(fullPath), cmt.commentData);
            cmt.commentData = fullPath;
            post.dataDirectory = mapPath;

            db.Posts.Add(post);
            db.Comments.Add(cmt);
            db.SaveChanges();

            return RedirectToAction("GetPostsInCategory", "Category", new { categoryID = categoryID });
        }
        
        public ActionResult ShowPost(int id, string postname)
        {
            if (string.IsNullOrEmpty(postname) || string.IsNullOrWhiteSpace(postname))
            {
                using (ForumDBEntities temp = new ForumDBEntities())
                {
                    Post posttemp = temp.Posts.SingleOrDefault(x => x.id == id);
                    if (posttemp == default(Post))
                    {
                        ViewBag.ErrorMessage = "This post is not found on forum!!!";
                        return View("Error");
                    }

                    string fullname = posttemp.postName;
                    fullname = fullname.Replace(' ', '-');

                    return RedirectToAction("ShowPost", "Post", new { id = id, postname = fullname });
                }
            }

            ForumDBEntities db = new ForumDBEntities();
            Post post = db.Posts.SingleOrDefault(x => x.id == id);
            if (post == default(Post)) 
            {
                ViewBag.ErrorMessage = "The post you access is not existed!!!";
                return View("Error");
            }

            if (Request.IsAuthenticated == false && post.Category.needAuthenticate == true)
            {
                ViewBag.ErrorMessage = "You don't have enough level access to view this post";
                return View("Error");
            }

            var comments = db.Comments.Where(x => x.Post.id == id);
            post.numView++;
            db.SaveChanges();
            ViewBag.PostID = id;
            ViewBag.NumComment = comments.Count();

            return View();
        }

        [ChildActionOnly]
        [OutputCache(Duration = 150, VaryByParam = "*")]
        public ActionResult GetCommentsInPost(int postID, int page)
        {
            ForumDBEntities db = new ForumDBEntities();
            Post post = db.Posts.SingleOrDefault(x => x.id == postID);
            if (post == default(Post))
            {
                ViewBag.ErrorMessage = "The post you access is not existed!!!";
                return View("Error");
            }

            if (Request.IsAuthenticated == false && post.Category.needAuthenticate == true)
            {
                ViewBag.ErrorMessage = "You don't have enough level access to view this post";
                return View("Error");
            }

            var comments = db.Comments.Where(x => x.Post.id == postID).OrderBy(x => x.id);
            var model = comments.Skip(10 * (page - 1)).Take(10);
            return View(model);
        }
    }
}