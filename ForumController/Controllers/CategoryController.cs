﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumModel;

namespace ForumController.Controllers
{
    public class CategoryController : Controller
    {
        [OutputCache(Duration = 120, Location = System.Web.UI.OutputCacheLocation.Client, VaryByParam = "categoryID", NoStore = true)]
        public ActionResult GetPostsInCategory(string categoryID)
        {
            ForumDBEntities db = new ForumDBEntities();
            var category = db.Categories.SingleOrDefault(x => x.shortname == categoryID);

            if (category == default(Category))
            {
                ViewBag.ErrorMessage = "This category: " + categoryID + " is not found on this forum!!!";
                return View("Error");
            }
            if (!Request.IsAuthenticated && category.needAuthenticate == true) 
            {
                return RedirectToAction("Login", "Account", new { returnUrl = "/Category/" + categoryID });
            }
            var model = category.Posts.OrderByDescending(x => x.datePost).Take(10);
            ViewBag.CategoryName = category.fullname;
            ViewBag.NumPost = model.Count();
            ViewBag.CategoryID = categoryID;

            return View(model);
        }
    }
}