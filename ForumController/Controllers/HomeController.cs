﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumModel;

namespace ForumController.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        [OutputCache(Duration = 3600, VaryByParam = "none")]
        public ActionResult PrivateCategory()
        {
            ForumDBEntities db = new ForumDBEntities();
            var model = db.Categories.Where(x => x.needAuthenticate == true);
            return View(model);
        }

        [ChildActionOnly]
        [OutputCache(Duration = 3600, VaryByParam = "none")]
        public ActionResult PublicCategory()
        {
            ForumDBEntities db = new ForumDBEntities();
            var model = db.Categories.Where(x => x.needAuthenticate == false);
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult NewestPost(string category)
        {
            ForumDBEntities db = new ForumDBEntities();
            var model = db.Posts.Where(x => x.categoryID == category).OrderByDescending(x => x.datePost).FirstOrDefault();
            return View(model);
        }
    }
}