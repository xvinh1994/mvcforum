﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using ForumModel;
using System.Web.Security;
using ForumController.ExternalClasses;

namespace ForumController.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdministratorController : Controller
    {
        public ActionResult Tools()
        {
            return View();
        }
        
        [OutputCache(Duration = int.MaxValue, VaryByParam = "none")]
        public ActionResult ChangeMemberAccount()
        {
            return View();
        }
        
        [OutputCache(Duration = 120, VaryByParam = "id")]
        public JsonResult EditAccount(int id)
        {
            ForumDBEntities db = new ForumDBEntities();
            if (!db.Members.Any(x => x.id == id))
            {
                return Json(new { status = "Error", message = "User not found!!!" }, JsonRequestBehavior.AllowGet);
            }

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null)
            {
                return Json(new { status = "Error", message = "Login to continue!!!" }, JsonRequestBehavior.AllowGet);
            }

            string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
            string curUserPrivileges = db.Members.Single(x => x.username == username).Membership.privileges.Trim();
            string targetPrivileges = db.Memberships.Single(x => x.memID == id).privileges.Trim();

            AdminHelper helper = new AdminHelper();

            if (helper.CompareAccessLevel(db, curUserPrivileges, targetPrivileges) > 0)
            {
                return Json(new { status = "Success", message = "Success" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = "Error", message = "You don't have enough access to perform this action!!!" }, JsonRequestBehavior.AllowGet);
        }
        
        [OutputCache(Duration = 120, VaryByParam = "id")]
        public JsonResult BanAccount(int id)
        {
            ForumDBEntities db = new ForumDBEntities();
            if (!db.Members.Any(x => x.id == id)) 
            {
                return Json(new { status = "Error", message = "User not found!!!" }, JsonRequestBehavior.AllowGet);
            }

            if (Request.Cookies[FormsAuthentication.FormsCookieName] == null) 
            {
                return Json(new { status = "Error", message = "Login to continue!!!" }, JsonRequestBehavior.AllowGet);
            }

            string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
            string curUserPrivileges = db.Members.Single(x => x.username == username).Membership.privileges.Trim();
            string targetPrivileges = db.Memberships.Single(x => x.memID == id).privileges.Trim();

            AdminHelper helper = new AdminHelper();

            if (helper.CompareAccessLevel(db, curUserPrivileges, targetPrivileges) > 0)
            {
                db.Members.Single(x => x.id == id).banned = true;
                db.SaveChanges();
            }
            else
            {
                return Json(new { status = "Error", message = "You don't have enough access to perform this action!!!" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = "Success", message = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OutputCache(Duration = 10, VaryByParam = "id")]
        public ActionResult ConfigureAccount(int id)
        {
            ForumDBEntities db = new ForumDBEntities();
            var model = db.Members.Single(x => x.id == id);
            ViewBag.MemberID = id;

            AdminHelper helper = new AdminHelper();
            string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
            Member user = db.Members.Single(x => x.username == username);
            ViewBag.PossibleGranted = helper.GetPossibleGrantedAccess(db, user.Membership.privileges);

            return View(model);
        }

        [HttpPost]
        public ActionResult ConfigureAccount(int id, string newPassword, string newAccess)
        {
            ForumDBEntities db = new ForumDBEntities();
            var member = db.Members.Single(x => x.id == id);

            if (!string.IsNullOrEmpty(newPassword) || !string.IsNullOrWhiteSpace(newPassword))
                member.pword = newPassword;
            member.Membership.privileges = db.Privileges.Single(x => x.shortAccess == newAccess).accessLevel;

            db.SaveChanges();

            return Json(new { status = "Success", message = "Success" });
        }
    }
}