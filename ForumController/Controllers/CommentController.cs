﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumModel;
using System.Web.Security;
using ForumController.ExternalClasses;

namespace ForumController.Controllers
{
    public class CommentController : Controller
    {
        [HttpGet]
        public ActionResult ReplyThread(int postID)
        {
            Comment model = new Comment() { commentPost = postID };
            ViewBag.PostID = postID;

            return View(model);
        }

        [HttpPost]
        public ActionResult ReplyThread(int postID, Comment cmt)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Please fulfill required data!!!");
                return View(cmt);
            }

            string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;

            ForumDBEntities db = new ForumDBEntities();
            cmt.commentPost = postID;
            cmt.commentMem = db.Members.Single(x => x.username == username).id;
            cmt.dateComment = DateTime.Now;

            CommentHelper helper = new CommentHelper();
            string filename = helper.GetFileName(cmt.id);
            string mapPath = db.Posts.Single(x => x.id == postID).dataDirectory;
            string fullPath = mapPath + "/" + filename;
            bool isOk = helper.WriteDataToHTML(Server.MapPath(fullPath), cmt.commentData);
            cmt.commentData = fullPath;

            db.Comments.Add(cmt);
            db.SaveChanges();

            return RedirectToAction("ShowPost", "Post", new { id = postID });
        }
    }
}