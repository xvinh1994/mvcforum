﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumModel;
using System.Web.Security;
using Facebook;
using System.Configuration;
using System.Net;
using ForumController.ExternalClasses;

namespace ForumController.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated) return RedirectToAction("Index", "Home");
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Member member, string returnUrl)
        {
            if (!ModelState.IsValid)
                return View(member);

            ForumDBEntities db = new ForumDBEntities();
            if (!db.Members.Any(x => x.username == member.username && x.pword == member.pword))
            {
                ModelState.AddModelError("Error", "Wrong username or password!!!");
                return View(member);
            }

            if (db.Members.Single(x => x.username == member.username).banned == true) 
            {
                ViewBag.ErrorMessage = "You have been banned from this forum!!!";
                return View("Error");
            }

            FormsAuthentication.SetAuthCookie(member.username, false);
            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [ChildActionOnly]
        public ActionResult ExternalLogin()
        {
            return View();
        }

        [Authorize(Roles = "User,Administrator")]
        [OutputCache(Duration = 3600, VaryByParam = "none", Location = System.Web.UI.OutputCacheLocation.Client, NoStore = true)]
        public ActionResult LoadAvatarForm()
        {
            string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
            Member mem;

            using (ForumDBEntities db = new ForumDBEntities())
            {
                mem = db.Members.SingleOrDefault(x => x.username == username);
            }

            return View(mem);
        }
        
        public ActionResult FaceBookLogin()
        {
            Uri loginUrl = FacebookHelper.LogUserIn();

            return Redirect(loginUrl.AbsoluteUri);
        }

        public ActionResult FacebookCallback(string code)
        {
            var fb = new FacebookClient();
            dynamic result = FacebookHelper.GetLoggedinUser(fb, code);

            var accessToken = result.access_token;

            // Store the access token in the session
            Session["AccessToken"] = accessToken;

            // update the facebook client with the access token so 
            // we can make requests on behalf of the user
            fb.AccessToken = accessToken;

            // Get the user's information
            dynamic me = fb.Get("me?fields=id,email");
            string email = me.email;
            string id = me.id;

            // Set the auth cookie
            FormsAuthentication.SetAuthCookie(email, false);

            //Get profile picture
            WebResponse response = null;
            string pictureUrl = string.Empty;
            WebRequest request = WebRequest.Create(string.Format("https://graph.facebook.com/{0}/picture?type=small", id));
            response = request.GetResponse();
            pictureUrl = response.ResponseUri.ToString();

            ForumDBEntities db = new ForumDBEntities();
            Member mem;
            if (!db.Members.Any(x => x.username == email))
            {
                mem = new Member { username = email, pword = email, avatarUrl = pictureUrl };
                db.Members.Add(mem);
                db.SaveChanges();
            }
            else
            {
                mem = db.Members.SingleOrDefault(x => x.username == email);
                if (mem.avatarUrl != pictureUrl)
                {
                    mem.avatarUrl = pictureUrl;
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult GoogleLogin()
        {
            var user = GoogleHelper.LogUserIn().Result;
            Member mem;
            ForumDBEntities db = new ForumDBEntities();
            if (!db.Members.Any(x => x.username == user.Emails[0].Value))
            {
                mem = new Member() { username = user.Emails[0].Value, pword = user.Emails[0].Value, avatarUrl = user.Image.Url };
                db.Members.Add(mem);
                db.SaveChanges();
            }
            else {
                mem = db.Members.SingleOrDefault(x => x.username == user.Emails[0].Value);
                if (mem.avatarUrl != user.Image.Url)
                {
                    mem.avatarUrl = user.Image.Url;
                    db.SaveChanges();
                }
            }

            FormsAuthentication.SetAuthCookie(mem.username, false);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult TwitterLogin()
        {
            var uri = TwitterHelper.LogUserIn();
            return new RedirectResult(uri.ToString(), false);
        }

        public ActionResult TwitterCallback(string oauth_token, string oauth_verifier)
        {
            var user = TwitterHelper.GetLoggedInUser(oauth_token, oauth_verifier);
            string username = user.Name + "_Twitter";
            Member mem;
            ForumDBEntities db = new ForumDBEntities();
            if (!db.Members.Any(x => x.username == username))
            {
                mem = new Member() { username = username, pword = username, avatarUrl = user.ProfileImageUrl };
                db.Members.Add(mem);
                db.SaveChanges();
            }
            else
            {
                mem = db.Members.SingleOrDefault(x => x.username == username);
                if (mem.avatarUrl != user.ProfileImageUrl)
                {
                    mem.avatarUrl = user.ProfileImageUrl;
                    db.SaveChanges();
                }
            }

            FormsAuthentication.SetAuthCookie(username, false);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            Member mem = new Member();
            return View(mem);
        }

        [HttpPost]
        public ActionResult Register(Member mem, bool acceptBox)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "Check your input!!!");
                return View(mem);
            }

            if (acceptBox == false)
            {
                ModelState.AddModelError("Error", "Must accept Term & Condition!!!");
                return View(mem);
            }

            ForumDBEntities db = new ForumDBEntities();
            db.Members.Add(mem);
            db.SaveChanges();

            return RedirectToAction("Login", "Account");
        }

        [Authorize]
        [OutputCache(Duration = 3600, VaryByParam = "none", Location = System.Web.UI.OutputCacheLocation.Client)]
        public ActionResult AboutMe()
        {
            string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
            ForumDBEntities db = new ForumDBEntities();
            Member mem = db.Members.SingleOrDefault(x => x.username == username);
            ViewBag.MemberID = mem.id;
            ViewBag.NumPost = mem.Posts.Count();

            return View();
        }
    }
}