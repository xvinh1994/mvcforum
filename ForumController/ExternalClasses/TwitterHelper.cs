﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using TweetSharp;

namespace ForumController.ExternalClasses
{
    public class TwitterHelper
    {
        public static Uri LogUserIn()
        {
            string key = ConfigurationManager.AppSettings["TwitterConsumerKey"];
            string secret = ConfigurationManager.AppSettings["TwitterConsumerSecret"];
            TwitterService service = new TwitterService(key, secret);
            OAuthRequestToken token = service.GetRequestToken(ConfigurationManager.AppSettings["TwitterCallbackUrl"]);
            Uri uri = service.GetAuthorizationUri(token);

            return uri;
        }

        public static TwitterUser GetLoggedInUser(string oauth_token, string oauth_verifier)
        {
            string key = ConfigurationManager.AppSettings["TwitterConsumerKey"];
            string secret = ConfigurationManager.AppSettings["TwitterConsumerSecret"];
            TwitterService service = new TwitterService(key, secret);
            var callbackToken = new OAuthRequestToken { Token = oauth_token };
            OAuthAccessToken accessToken = service.GetAccessToken(callbackToken, oauth_verifier);
            service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);
            TwitterUser user = service.VerifyCredentials(new VerifyCredentialsOptions());

            return user;
        }
    }
}