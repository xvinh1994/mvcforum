﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Facebook;
using System.Configuration;

namespace ForumController.ExternalClasses
{
    public class FacebookHelper
    {
        private static Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(HttpContext.Current.Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = ConfigurationManager.AppSettings["FacebookCallbackUrl"];

                return uriBuilder.Uri;
            }
        }

        public static Uri LogUserIn()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["FacebookAppID"],
                client_secret = ConfigurationManager.AppSettings["FacebookAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                respone_type = "code",
                scope = "email"
            });

            return loginUrl;
        }

        public static dynamic GetLoggedinUser(FacebookClient fb, string code)
        {
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = ConfigurationManager.AppSettings["FacebookAppID"],
                client_secret = ConfigurationManager.AppSettings["FacebookAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code
            });

            return result;
        }
    }
}