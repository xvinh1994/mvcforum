﻿using Google.Apis.Auth.OAuth2;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Google.Apis.Oauth2.v2;
using System.Threading;
using Google.Apis.Util.Store;
using System.Threading.Tasks;
using Google.Apis.Services;
using Google.Apis.Plus.v1;
using System.Web.Hosting;

namespace ForumController.ExternalClasses
{
    public class GoogleHelper
    {
        public static async Task<Google.Apis.Plus.v1.Data.Person> LogUserIn()
        {
            UserCredential credential;
            using (var stream = new FileStream(Path.GetFullPath(HostingEnvironment.MapPath("~/App_Data/client_secret.json")), FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[] { PlusService.Scope.UserinfoEmail, PlusService.Scope.PlusLogin },
                    "user", CancellationToken.None, null);
            }

            var service = new PlusService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "ForumLogin"
            });

            var userinfo = await service.People.Get("me").ExecuteAsync();
            return userinfo;
        }
    }
}