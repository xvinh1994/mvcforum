﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;

namespace ForumController.ExternalClasses
{
    public class CommentHelper
    {
        public string GetFileName(int commentID)
        {
            string result = "{0}_{1}.html";
            result = string.Format(result, DateTime.Now.ToString("MMM_dd_yyyy"), commentID.ToString());
            return result;
        }

        public bool WriteDataToHTML(string path, string data)
        {
            //convert custom tag into html tag
            data = convertToBoldTag(data);
            data = convertToItalicTag(data);
            data = convertToUnderlineTag(data);
            data = convertToFontSizeTag(data);
            data = convertToFontStyleTag(data);

            //write to file
            StreamWriter writer = new StreamWriter(path);

            try
            {
                writer.WriteLine(data);
                writer.Flush();
                writer.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private string convertToFontStyleTag(string data)
        {
            data = data.Replace("[/STYLE]", "</span>");
            Regex regex = new Regex(@"\[STYLE=\[");
            data = regex.Replace(data, "<span style=\"font-family:");
            Regex regex2 = new Regex(@"\]\]([A-Za-z0-9]+)");
            Match match = regex2.Match(data);
            string tmp = match.Value.Replace("]]", "\">");
            data = regex2.Replace(data, tmp);

            return data;
        }

        public string CreateDirectoryForNewPost(int postID, HttpServerUtilityBase server)
        {
            string subFolder = "Year_" + DateTime.Now.Year + "/Month_" + DateTime.Now.Month + "/Day_" + DateTime.Now.Day + "/Post_" + postID;
            string path = server.MapPath("~/data/");
            Directory.CreateDirectory(path + subFolder);
            return "~/data/" + subFolder;
        }

        private string convertToFontSizeTag(string data)
        {
            data = data.Replace("[/SIZE]", "</span>");
            Regex regex = new Regex(@"\[SIZE=");
            data = regex.Replace(data, "<span style=\"font-size:");
            Regex regex2 = new Regex(@"\]([A-Za-z0-9]+)");
            Match match = regex2.Match(data);
            string tmp = match.Value.Replace("]", "px\">");
            data = regex2.Replace(data, tmp);

            return data;
        }

        private string convertToUnderlineTag(string data)
        {
            data = data.Replace("[/U]", "</u>");
            data = data.Replace("[U]", "<u>");
            return data;
        }

        private string convertToBoldTag(string data)
        {
            data = data.Replace("[/B]", "</strong>");
            data = data.Replace("[B]", "<strong>");
            return data;
        }

        private string convertToItalicTag(string data)
        {
            data = data.Replace("[/I]", "</i>");
            data = data.Replace("[I]", "<i>");
            return data;
        }
    }
}