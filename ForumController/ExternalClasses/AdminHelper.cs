﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ForumModel;

namespace ForumController.ExternalClasses
{
    public class AdminHelper
    {
        /// <summary>
        /// Compare if 2 highest priorities are same
        /// </summary>
        /// <param name="db">database object</param>
        /// <param name="curUserPrivileges">previleges of current logged-in user</param>
        /// <param name="targetPrivileges">previleges of target user</param>
        /// <returns>return negative number if curUser has lower priority; positive number if curUser has higher priority; 0(zero) if same priority</returns>
        public int CompareAccessLevel(ForumDBEntities db, string curUserPrivileges, string targetPrivileges)
        {
            string[] curUser = curUserPrivileges.Split(';');
            string[] target = targetPrivileges.Split(';');
            var serverPrivileges = db.Privileges;

            int maxCur = maxPriority(serverPrivileges, curUser);
            int maxTarget = maxPriority(serverPrivileges, target);

            return maxCur - maxTarget;
        }

        private int maxPriority(IEnumerable<Privilege> serverPrivileges, string[] previleges)
        {
            int max = 0;

            foreach (var previlege in previleges)
            {
                var item = serverPrivileges.Single(x => x.accessLevel.Trim() == previlege.Trim());
                int cur = item.priority.Value;
                max = max < cur ? cur : max;
            }

            return max;
        }

        public IEnumerable<KeyValuePair<string, string>> GetPossibleGrantedAccess(ForumDBEntities db, string privileges)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            var serverPrivileges = db.Privileges;
            int max = maxPriority(serverPrivileges, privileges.Split(';'));
            var granted = db.Privileges.Where(x => x.priority < max);

            foreach (var item in granted)
            {
                result.Add(new KeyValuePair<string, string>(item.shortAccess, item.accessLevel.Trim()));
            }

            return result;
        }
    }
}