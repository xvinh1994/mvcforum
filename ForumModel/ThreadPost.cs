﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumModel
{
    public class ThreadPost
    {
        public Post post { get; set; }
        public Comment comment { get; set; }
        [Display(Name = "Icon for thread")]
        public string[] icons { get
            {
                List<string> data = new List<string>();
                using (ForumDBEntities db = new ForumDBEntities())
                {
                    var icon = db.PostIcons;
                    foreach (var item in icon)
                    {
                        data.Add(item.iconUrl);
                    }
                }

                return data.ToArray();
            }
        }
    }
}
