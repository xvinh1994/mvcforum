﻿function loadChangeMemberPage() {
    var viewSection = document.getElementById('viewSection');
    viewSection.innerHTML = "";
    $("#viewSection").load('/Administrator/ChangeMemberAccount');
}

function searchUser() {
    var divShowUser = document.getElementById('showUser');
    var pattern = document.getElementById('txtUsername').value;
    divShowUser.innerHTML = "";
    $("#showUser").load('/Member/FindAllMember?pattern=' + pattern);
}

function banAccount(id) {
    var urlLink = "/Administrator/BanAccount?id=" + id;
    $.ajax({
        url: urlLink,
        type: "GET",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        cache: false,
        success: function (data) {
            var status = "Status: " + data.status;
            var message = data.message;
            alert(status + ". " + message);
            searchUser();
        },
        error: function () {
            alert("An unidentified error occurred!!!");
        }
    });
}

function editAccount(id) {
    var urlLink = "/Administrator/EditAccount?id=" + id;
    $.ajax({
        url: urlLink,
        type: "GET",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        cache: false,
        success: function (data) {
            if (data.status == "Success") {
                $("#showUser").load('/Administrator/ConfigureAccount?id=' + id);
            }
            else {
                var status = "Status: " + data.status;
                var message = data.message;
                alert(status + ". " + message);
            }
        },
        error: function () {
            alert("An unidentified error occurred!!!");
        }
    });
}

function showPassword(ele1, ele2) {
    var target = document.getElementById(ele2);
    var btn = document.getElementById(ele1);

    if (btn.innerHTML == "Show") {
        target.setAttribute('type', 'text');
        btn.innerHTML = "Hide";
    }
    else {
        target.setAttribute('type', 'password');
        btn.innerHTML = "Show";
    }
}

function grantNewAccess() {
    var selAccess = document.getElementById('selAccess');
    var newAccess = document.getElementById('newAccess');
    var sel = selAccess.options[selAccess.selectedIndex].value;

    if (sel == "--") return;
    newAccess.value = sel;
}

function submitEditAccount() {
    $("#editAccountForm").submit(function () {
        var f = $("#editAccountForm");
        var action = f.attr("action");
        var serializedData = f.serialize();
        $.post(action, serializedData, function (data) {
            if (data.status == "Success") {
                alert("Edit Done!!!");
                searchUser();
            }
            else alert("An unidentitfied error occurred!!!");
        });
        return false;
    });

    jQuery().ajaxStart(function () {
        $("#editAccountForm").fadeOut("slow");
    });
    jQuery().ajaxStop(function () {
        $("#editAccountForm").fadeIn("fast");
    });
}