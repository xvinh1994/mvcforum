﻿function changeIconID(iconID) {
    var selectedIconID = document.getElementById('selectedIconID');
    selectedIconID.value = iconID;
}

function boldIt() {
    $('#inputText')
        .selection('insert', { text: '[B]', mode: 'before' })
        .selection('insert', { text: '[/B]', mode: 'after' });
}

function italicIt() {
    $('#inputText')
        .selection('insert', { text: '[I]', mode: 'before' })
        .selection('insert', { text: '[/I]', mode: 'after' });
}

function underlineIt() {
    $('#inputText')
        .selection('insert', { text: '[U]', mode: 'before' })
        .selection('insert', { text: '[/U]', mode: 'after' });
}

function changeFontSize() {
    var sel = document.getElementById('selFontSize');
    var selVal = sel.options[sel.selectedIndex].value;
    sel.value = '--';

    if (selVal == '--') return;
    var txt = '[SIZE=' + selVal + ']';
    $('#inputText')
        .selection('insert', { text: txt, mode: 'before' })
        .selection('insert', { text: '[/SIZE]', mode: 'after' });
}

function changeFontStyle() {
    var selStyle = document.getElementById('selFontStyle');
    var selStyleVal = selStyle.options[selStyle.selectedIndex].value;
    selStyle.value = '--';

    if (selStyleVal == '--') return;
    var txt = '[STYLE=[' + selStyleVal + ']]';
    $('#inputText')
        .selection('insert', { text: txt, mode: 'before' })
        .selection('insert', { text: '[/STYLE]', mode: 'after' });
}

function loadPageInPost(postID, page) {
    var divPostInCategory = document.getElementById('commentsInPost');
    divPostInCategory.innerHTML = '';
    $("#commentsInPost").load("/Post/GetPostByCategory?categoryID=" + categoryID + "&page=" + page);
}

function loadPrevPage() {
    var btnPrevPage = document.getElementById('prevPage');
    var page = btnPrevPage.value;

    var postID = document.getElementById('postID').value;

    loadPageInPost(postID, page);

    page = page - 1;
    btnPrevPage.value = page;
    if (page == 0) btnPrevPage.disabled = true;

    var btnNextPage = document.getElementById('nextPage');
    btnNextPage.value = page + 2;
}

function loadNextPage() {
    var btnNextPage = document.getElementById('nextPage');
    var page = btnNextPage.value;

    var postID = document.getElementById('postID').value;
    var maxPage = document.getElementById('maxPage').value;

    loadPage(postID, page);

    page = page + 1;
    btnNextPage.value = page;
    if (page > maxPage) btnNextPage.disabled = true;

    var btnPrevPage = document.getElementById('prevPage');
    btnPrevPage.value = page - 2;
}

function gotoPage() {
    var curPage = document.getElementById('curPage').value;
    var maxPage = document.getElementById('maxPage').value;

    if (curPage > 0 && curPage <= maxPage) {
        var postID = document.getElementById('postID').value;
        loadPage(postID, curPage);
    }
}