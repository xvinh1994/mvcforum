﻿function loadDefaultAvatar() {
    var avatarImg = document.getElementById('avatarImg');
    avatarImg.src = '/img/UserAvatar/default-user.png';
}

function firstLoadPagePost() {
    var btnPrevPage = document.getElementById('prevPage');
    var page = btnPrevPage.value;

    if (page <= 0) btnPrevPage.disabled = true;

    var maxPage = document.getElementById('maxPage').value;
    var btnNextPage = document.getElementById('nextPage');
    page = btnNextPage.value;

    if (page > maxPage) btnNextPage.disabled = true;
}

function loadPage(memberID, page) {
    var divPostLoaded = document.getElementById('postLoaded');
    divPostLoaded.innerHTML = '';
    $("#postLoaded").load("/Post/GetPostByMember?memberID=" + memberID + "&page=" + page);
}

function loadPrevPagePost() {
    var btnPrevPage = document.getElementById('prevPage');
    var page = btnPrevPage.value;

    var memberID = document.getElementById('userID').value;

    loadPage(memberID, page);

    page = page - 1;
    btnPrevPage.value = page;
    if (page == 0) btnPrevPage.disabled = true;

    var btnNextPage = document.getElementById('nextPage');
    btnNextPage.value = page + 2;
}

function loadNextPagePost() {
    var btnNextPage = document.getElementById('nextPage');
    var page = btnNextPage.value;

    var memberID = document.getElementById('userID').value;
    var maxPage = document.getElementById('maxPage').value;

    loadPage(memberID, page);

    page = page + 1;
    btnNextPage.value = page;
    if (page > maxPage) btnNextPage.disabled = true;

    var btnPrevPage = document.getElementById('prevPage');
    btnPrevPage.value = page - 2;
}

function gotoPagePost() {
    var curPage = document.getElementById('curPage').value;
    var maxPage = document.getElementById('maxPage').value;

    if (curPage > 0 && curPage <= maxPage) {
        var memberID = document.getElementById('userID').value;
        loadPage(memberID, curPage);
    }
}