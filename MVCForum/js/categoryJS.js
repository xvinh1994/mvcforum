﻿
function loadPageInCategory(categoryID, page) {
    var divPostInCategory = document.getElementById('postsInCategory');
    divPostInCategory.innerHTML = '';
    $("#postsInCategory").load("/Post/GetPostByCategory?categoryID=" + categoryID + "&page=" + page);
}

function loadPrevPagePostInCategory() {
    var btnPrevPage = document.getElementById('prevPage');
    var page = btnPrevPage.value;

    var categoryID = document.getElementById('categoryID').value;

    loadPageInCategory(categoryID, page);

    page = page - 1;
    btnPrevPage.value = page;
    if (page == 0) btnPrevPage.disabled = true;

    var btnNextPage = document.getElementById('nextPage');
    btnNextPage.value = page + 2;
}

function loadNextPagePostInCategory() {
    var btnNextPage = document.getElementById('nextPage');
    var page = btnNextPage.value;

    var categoryID = document.getElementById('categoryID').value;
    var maxPage = document.getElementById('maxPage').value;

    loadPageInCategory(categoryID, page);

    page = page + 1;
    btnNextPage.value = page;
    if (page > maxPage) btnNextPage.disabled = true;

    var btnPrevPage = document.getElementById('prevPage');
    btnPrevPage.value = page - 2;
}

function gotoPagePostInCategory() {
    var curPage = document.getElementById('curPage').value;
    var maxPage = document.getElementById('maxPage').value;

    if (curPage > 0 && curPage <= maxPage) {
        var categoryID = document.getElementById('CategoryID').value;
        loadPage(categoryID, curPage);
    }
}
