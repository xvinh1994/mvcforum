﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVCForum
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "CategoryDefault",
                url: "Category/{categoryID}",
                namespaces: new[] { "ForumController" },
                defaults: new { controller = "Category", action = "GetPostsInCategory", categoryID = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CreatePost",
                url: "Post/Create/{categoryID}",
                namespaces: new[] { "ForumController" },
                defaults: new { controller = "Post", action = "Create" }
            );

            routes.MapRoute(
                name: "ShowPost",
                url: "Post/{id}/{postname}",
                namespaces: new[] { "ForumController" },
                defaults: new { controller = "Post", action = "ShowPost", postname = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "FindMember",
                url: "Member/{id}",
                namespaces: new[] { "ForumController" },
                constraints: new { id = @"\d+" },
                defaults: new { controller = "Member", action = "ShowMember", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "FindAllMember",
                url: "Member/FindAllMember",
                namespaces: new[] { "ForumController" },
                defaults: new { controller = "Member", action = "FindAllMember"}
            );

            routes.MapRoute(
                name: "AdministratorDefault",
                url: "Administrator/{action}",
                namespaces: new[] { "ForumController" },
                defaults: new { controller = "Administrator", action = "Tools" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                namespaces: new[] { "ForumController" },
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
